<?php 

$superMenu = get_field('menu' , 'option');
    // echo '<pre>';
    // var_dump($superMenu);
    // echo '</pre>';
?> 


<?php if( have_rows('menu', 'option') ):

    while( have_rows('menu', 'option') ): the_row(); 

        $id = get_sub_field('menu_id');
        $content = get_sub_field('content');
        ?>

        <div class="nav-panel" id="menu-item-<?php echo $id; ?>" style="display: none;">
            <div class="container">
                <div><?php echo $content; ?></div>
            </div>
        </div>

    <?php endwhile;

endif; ?>



<?php
if ( !wp_is_mobile() ) : ?>
    <script type="text/javascript">
        var hoverTimeout, keepOpen = false, stayOpen = $('#menu-item-45'); 
        
        $(document).on('mouseover','.menu-item-45',function(){
            keepOpen = true;
            stayOpen.addClass('show');
            console.log('Add class show');
        }).on('mouseleave','.menu-item-45',function(){
            keepOpen = false;
            hoverTimeout = setTimeout(function(){
                if(!keepOpen){
                    stayOpen.removeClass('show'); 
                    console.log('Remove class stay open');  
                }
            },200);

        }).on('mouseover','#menu-item-45',function(){
            keepOpen = true;
            stayOpen.addClass('show');
            console.log('Add class show');
        }).on('mouseleave','#menu-item-45',function(){
            keepOpen = false;
            stayOpen.removeClass('show');
        });

        

 
    </script>
<?php endif; ?>