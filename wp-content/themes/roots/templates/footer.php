<?php $privacyPolicy = get_page_by_title( 'Privacy Policy' ); ?>

<footer class="site-footer content-info" role="contentinfo">
    <div class="footer-upper">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <?php dynamic_sidebar( 'footer-a' ); ?>
                </div>
                <div class="col-sm-3">
                    <?php dynamic_sidebar( 'footer-b' ); ?>
                </div>
                <div class="col-sm-3">
                    <?php dynamic_sidebar( 'footer-c' ); ?>
                </div>
                <div class="col-sm-3">
                    <?php dynamic_sidebar( 'footer-d' ); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <?php if(is_front_page()): ?>
                <?php bloginfo( 'name' ); ?> &copy; <?php echo date( 'Y' ); ?> - <a href="https://www.lemonadestand.org/" target="_blank">Lemonade Stand | <a href="/<?php echo $privacyPolicy->post_name; ?>"><?php echo $privacyPolicy->post_title; ?></a>
            <?php else: ?>
                <?php bloginfo( 'name' ); ?> &copy; <?php echo date( 'Y' ); ?>
            <?php endif; ?>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>
