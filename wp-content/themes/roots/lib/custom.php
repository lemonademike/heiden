<?php
/**
 * Custom functions
 *
 * More for misc. functions, not a catch all
 *
 * Use:
 * image-sizes.php - to manage image sizes
 * widgets.php     - to manage widget areas
 */

remove_filter( 'the_content', 'wpautop' );

remove_filter( 'the_excerpt', 'wpautop' );


function print_menu_shortcode($atts, $content = null) {
    extract(shortcode_atts(array( 'name' => null, ), $atts));
    return wp_nav_menu( array( 'menu' => $name, 'echo' => false ) );
}
add_shortcode('menu', 'print_menu_shortcode');