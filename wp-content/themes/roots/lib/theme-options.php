<?php

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
		'autoload'    => true // all options saved on this page are autoloaded. Use get_option() instead of get_field() for these fields. *Note - prefix the field name with 'options_' ex. (options_site_logo)
	));

    acf_add_options_sub_page(array(
        'page_title'    => 'Super Menu Settings',
        'menu_title'    => 'Super Menu',
        'parent_slug'   => 'theme-general-settings',
    ));

}

