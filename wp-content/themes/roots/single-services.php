<div class="row">
	<div class="col-md-7 wide-gap-left">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>
		<?php endif; ?>
	</div>
	<div class="col-md-4 wide-gap-right">
		<div class="well">
			<h3>Free Consultation</h3>
			<?php echo do_shortcode('[gravityform id="2" title="false" description="false"]'); ?>
		</div>
	</div>
</div>




