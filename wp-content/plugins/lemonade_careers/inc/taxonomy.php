<?php
//Career Type
function career_type_init() {
	register_taxonomy(
		'career_type',
		'careers',
		array(
			'label' => __( 'Career Type' ),
			'rewrite' => array( 
			'slug' => 'career_type',
			),
		'hierarchical' => true,
		)
	);
}
add_action( 'init', 'career_type_init' ); 