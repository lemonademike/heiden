<?php
function lemonade_create_careers() {
	$labels = array(
		'name'                => 'Careers',
		'singular_name'       => 'Career',
		'menu_name'           => 'Careers',
		'parent_item_colon'   => 'Parent Career:',
		'all_items'           => 'All Careers',
		'view_item'           => 'View Career',
		'add_new_item'        => 'Add New Career',
		'add_new'             => 'New Career',
		'edit_item'           => 'Edit Career',
		'update_item'         => 'Update Career',
		'search_items'        => 'Search Careers',
		'not_found'           => 'No Careers found',
		'not_found_in_trash'  => 'No Careers found in Trash',
	);

	$args = array(
		'label'               => 'Careers',
		'description'         => 'Careers post type',
		'labels'              => $labels,
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 6,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'capability_type'     => 'page',
		'supports'            => array( 'title', 'excerpt', 'thumbnail', 'revisions', 'page-attributes', 'editor'),
		'menu_icon' => plugins_url( 'lemonade_icon.png', __FILE__ ),
	);

	register_post_type( 'careers', $args );
}
add_action( 'init', 'lemonade_create_careers', 0 );