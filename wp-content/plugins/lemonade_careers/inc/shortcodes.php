<?php
/* Shortcodes */
function lemonade_careers_shortcode( $atts ) {
	extract( shortcode_atts( array(
		'order'						=> 'ASC',
		'orderby'					=> 'menu_order',
		'meta_key'				=> '',
		'posts_per_page'	=> '-1', 
		'display'					=> 'standard',
		'terms_list' 			=> '',
		'by_term'					=> '0',
		'career_type' 		=> '',
	), $atts ) );

	$terms_array = explode(",", $career_type);

	$db_args = array(
		'post_type' 			=> 'careers',
		'order'						=> $order,
		'orderby'					=> $orderby,
		'meta_key'				=> $meta_key,
		'posts_per_page' 	=> $posts_per_page, 
		'by_term'					=> $by_term,
		'career_type' 		=> $career_type,
	);


	if($career_type != ""){
		$db_args['tax_query'][] = array(
			array(
				'taxonomy' => 'career_type',
				'field'    => 'slug',
				'terms'    => $terms_array,
			),
		);		
	}

	$careers_loop = new WP_Query( $db_args );

	$content = '';

	if($careers_loop->have_posts()) {
		switch($display) {	

		case "accordion":
			$content .= '<div class="panel-group" id="careers-accordion">';

			$i = 0;

			while( $careers_loop->have_posts() ) : $careers_loop->the_post();
			
				if($i == 0) { $first = "in"; } else { $first = ''; }
			
				$content_filtered = get_the_content();
				$content_filtered = apply_filters('the_content', $content_filtered);
				$content_filtered = str_replace(']]>', ']]&gt;', $content_filtered);
				
			    $content .= '<div class="panel panel-default">';
				$content .= '<div class="panel-heading">';
				$content .= '<h3 class="panel-title">';
				$content .= '<a class="accordion-toggle" data-toggle="collapse" data-parent="#careers-accordion" href="#collapse'.$i.'">';
				$content .= get_the_title();
				$content .= '</a>';					
				$content .= '</h3>';
				$content .= '</div>'; // end .panel-heading
				$content .= '<div id="collapse'.$i.'" class="panel-collapse collapse '.$first.'">';
				$content .= '<div class="panel-body">'.$content_filtered.'</div>';
				$content .= '</div>'; // end .collapse
				$content .= '</div>'; // end .panel

				$i++;

			endwhile;

			$content .= "</div>"; // end .panel-group

			break;


			case "standard":

				$content .= '<div class="careers-wrapper">';

				while( $careers_loop->have_posts() ) : $careers_loop->the_post();

					$content .= '<div class="medium-6 large-3 columns">';
					$content .= '<div class="careers-single">';
					$content .= '<a href="'.get_permalink().'">';
					$content .= get_the_post_thumbnail($post->ID, 'doc-thumb');
					$content .= '<p>'.get_the_title().'</p>';
					$content .= '</a>';
					$content .= '</div>';
					$content .= '</div>';

				endwhile;

				$content .= '</div>';

				break;



			case "content":

				$content .= '<div class="careers-wrapper">';

				while( $careers_loop->have_posts() ) : $careers_loop->the_post();

					$content_filtered = get_the_content();
					$content_filtered = apply_filters('the_content', $content_filtered);
					$content_filtered = str_replace(']]>', ']]&gt;', $content_filtered);

					$content .= '<div class="careers-single">';
					$content .= '<h3 class="careers-title">'.get_the_title().'</h3>';
					$content .= '<div class="careers-content">'.$content_filtered.'</div>';
					$content .= '</div>';
				endwhile;

				$content .= '</div>';

				break;

			case "excerpt":

				$content .= '<div class="careers-wrapper">';

				while( $careers_loop->have_posts() ) : $careers_loop->the_post();
					$content .= '<div class="careers-single">';
					$content .= '<h3 class="careers-title"><a href=".get_permalink().">'.get_the_title().'</a></h3>';
					$content .= '<div class="careers-excerpt">'.get_the_excerpt().'</div>';
					$content .= '</div>';
				endwhile;

				$content .= '</div>';

				break;

			case "list":

				$content .= '<ul class="careers-wrapper">';

				while( $careers_loop->have_posts() ) : $careers_loop->the_post();
					$content .= '<li class="careers-single">';
					$content .= '<h3 class="careers-title"><a href="'.get_permalink().'">'.get_the_title().'</a></h3>';
					$content .= '</li>';
				endwhile;

				$content .= '</ul>';

				break;
		}
			
	}

	wp_reset_postdata();
	return $content;
}
add_shortcode( 'lemonade_careers', 'lemonade_careers_shortcode' );