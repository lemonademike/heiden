<?php
/*
Plugin Name: Careers - Lemonade Stand
Plugin URI: https://www.lemonadestand.org/
Description: Careers custom post type.
Version: 1.0
Author: Lemonade Stand
Author URI: https://www.lemonadestand.org/
*/

include("inc/post_type.php");
include("inc/shortcodes.php");
include("inc/taxonomy.php");


/* rename labels */
function lemonade_careers_title_alter( $title ) {
    $screen = get_current_screen();
    if ( $screen->post_type == "careers") {
        $title = 'Enter Career Title Here';
    }
    return $title;
}
add_filter( 'enter_title_here', 'lemonade_careers_title_alter' );

