<?php
/* Shortcodes */
function lemonade_news_shortcode( $atts ) {
	extract( shortcode_atts( array(
		'order'						=> 'ASC',
		'orderby'					=> 'menu_order',
		'meta_key'				=> '',
		'posts_per_page'	=> '-1', 
		'display'					=> 'standard',
		'terms_list' 			=> '',
		'by_term'					=> '0',
		'news_type' 		=> '',
	), $atts ) );

	$terms_array = explode(",", $news_type);

	$db_args = array(
		'post_type' 			=> 'news',
		'order'						=> $order,
		'orderby'					=> $orderby,
		'meta_key'				=> $meta_key,
		'posts_per_page' 	=> $posts_per_page, 
		'by_term'					=> $by_term,
		'news_type' 		=> $news_type,
	);


	if($news_type != ""){
		$db_args['tax_query'][] = array(
			array(
				'taxonomy' => 'news_type',
				'field'    => 'slug',
				'terms'    => $terms_array,
			),
		);		
	}

	$news_loop = new WP_Query( $db_args );

	$content = '';

	if($news_loop->have_posts()) {
		switch($display) {	

		case "accordion":
			$content .= '<div class="panel-group" id="news-accordion">';

			$i = 0;

			while( $news_loop->have_posts() ) : $news_loop->the_post();
			
				if($i == 0) { $first = "in"; } else { $first = ''; }
			
				$content_filtered = get_the_content();
				$content_filtered = apply_filters('the_content', $content_filtered);
				$content_filtered = str_replace(']]>', ']]&gt;', $content_filtered);
				
			    $content .= '<div class="panel panel-default">';
				$content .= '<div class="panel-heading">';
				$content .= '<h3 class="panel-title">';
				$content .= '<a class="accordion-toggle" data-toggle="collapse" data-parent="#news-accordion" href="#collapse'.$i.'">';
				$content .= get_the_title();
				$content .= '</a>';					
				$content .= '</h3>';
				$content .= '</div>'; // end .panel-heading
				$content .= '<div id="collapse'.$i.'" class="panel-collapse collapse '.$first.'">';
				$content .= '<div class="panel-body">'.$content_filtered.'</div>';
				$content .= '</div>'; // end .collapse
				$content .= '</div>'; // end .panel

				$i++;

			endwhile;

			$content .= "</div>"; // end .panel-group

			break;


			case "standard":

				$content .= '<div class="news-wrapper">';

				while( $news_loop->have_posts() ) : $news_loop->the_post();

					$content .= '<div class="medium-6 large-3 columns">';
					$content .= '<div class="news-single">';
					$content .= '<a href="'.get_permalink().'">';
					$content .= get_the_post_thumbnail($post->ID, 'doc-thumb');
					$content .= '<p>'.get_the_title().'</p>';
					$content .= '</a>';
					$content .= '</div>';
					$content .= '</div>';

				endwhile;

				$content .= '</div>';

				break;



			case "content":

				$content .= '<div class="news-wrapper">';

				while( $news_loop->have_posts() ) : $news_loop->the_post();

					$content_filtered = get_the_content();
					$content_filtered = apply_filters('the_content', $content_filtered);
					$content_filtered = str_replace(']]>', ']]&gt;', $content_filtered);

					$content .= '<div class="news-single">';
					$content .= '<h3 class="news-title">'.get_the_title().'</h3>';
					$content .= '<div class="news-content">'.$content_filtered.'</div>';
					$content .= '</div>';
				endwhile;

				$content .= '</div>';

				break;

			case "excerpt":

				$content .= '<div class="news-wrapper">';

				while( $news_loop->have_posts() ) : $news_loop->the_post();
					$content .= '<div class="news-single">';
					$content .= '<h3 class="news-title"><a href="'.get_permalink().'">'.get_the_title().'</a></h3>';
					$content .= '<div class="news-excerpt">'.get_the_excerpt().'</div>';
					$content .= '</div>';
				endwhile;

				$content .= '</div>';

				break;

			case "list":

				$content .= '<ul class="news-wrapper">';

				while( $news_loop->have_posts() ) : $news_loop->the_post();
					$content .= '<li class="news-single">';
					$content .= '<a class="news-title" href="'.get_permalink().'"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>'.get_the_title().'</a>';
					$content .= '</li>';
				endwhile;

				$content .= '</ul>';

				break;

				case "ramon":

				$content .= '<ul class="medium-block-grid-2 large-block-grid-4 small-block-grid-1 no-margins">';

				while( $news_loop->have_posts() ) : $news_loop->the_post();
					$content .= '<li class="news-single">';
					$content .= '<h4 class="title highlight-primary">'.get_the_title().'</h4>';
					$content .= '<p class="desc">'.get_the_excerpt().'</p>';
					$content .= '<p class="none"><a class="no-underline" href="'.get_permalink().'"> Learn More <i class="fas fa-long-arrow-alt-right"></i></a></p>';
					$content .= '</li>';
				endwhile;

				$content .= '</ul>';

				break;
		}
			
	}

	wp_reset_postdata();
	return $content;
}
add_shortcode( 'lemonade_news', 'lemonade_news_shortcode' );