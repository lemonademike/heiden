<?php
//News Type
function news_type_init() {
	register_taxonomy(
		'news_type',
		'news',
		array(
			'label' => __( 'News Type' ),
			'rewrite' => array( 
			'slug' => 'news_type',
			),
		'hierarchical' => true,
		)
	);
}
add_action( 'init', 'news_type_init' ); 