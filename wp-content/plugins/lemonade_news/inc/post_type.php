<?php
function lemonade_create_news() {
	$labels = array(
		'name'                => 'News',
		'singular_name'       => 'News',
		'menu_name'           => 'News',
		'parent_item_colon'   => 'Parent News:',
		'all_items'           => 'All News',
		'view_item'           => 'View News',
		'add_new_item'        => 'Add New News',
		'add_new'             => 'New News',
		'edit_item'           => 'Edit News',
		'update_item'         => 'Update News',
		'search_items'        => 'Search News',
		'not_found'           => 'No News found',
		'not_found_in_trash'  => 'No News found in Trash',
	);

	$args = array(
		'label'               => 'News',
		'description'         => 'News post type',
		'labels'              => $labels,
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 6,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'capability_type'     => 'page',
		'supports'            => array( 'title', 'excerpt', 'thumbnail', 'revisions', 'page-attributes', 'editor'),
		'menu_icon' => plugins_url( 'lemonade_icon.png', __FILE__ ),
	);

	register_post_type( 'news', $args );
}
add_action( 'init', 'lemonade_create_news', 0 );